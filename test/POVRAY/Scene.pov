#include "colors.inc"
#include "woods.inc"

#macro makeHoles(i)
    #declare prod1 = -0.1;
    #declare prod2 = 1.1;
    #declare psize = (1/3) / (pow(3,i));

    #declare a = (1/3) / (pow(3,i));
    #while (a<=1)
        #declare b = (1/3) / (pow(3,i));
        #while (b<=1)
            box {<prod1, b, a> <prod2, b+psize, a+psize>} 
            box {<b, prod1, a> <b+psize, prod2, a+psize>} 
            box {<b, a, prod1> <b+psize, a+psize, prod2>} 

            #declare b = b + pow((1/3),i);
        #end

        #declare a = a + pow((1/3),i);
    #end
#end

#macro makeSpongeNeg(n)
    #declare i = 0;
    #while (i<=n)
        makeHoles(i)

        #declare i = i + 1;
    #end
#end

#macro makeSponge(n)
    difference {
        object {box {<0, 0, 0>, <1, 1, 1>}}
        union{makeSpongeNeg(n)} 
    }
#end

box{
    <-10,-1,-10>, <10,0,10>
    texture{T_Wood20} 
    finish{reflection 0.2} 
}

#declare j = 0;
#while (j<4)
    object{
        makeSponge(j)
        texture{T_Wood10} 
        translate <j*-1.2, 0, 0>
    } 
    #debug "Test"

    #declare j = j + 1;
#end

background{color White}
light_source{<0,8,4> color White}

camera{
    location <2, 3, 4>
    angle 50
    right x
    look_at <-1, 0, 0>
}