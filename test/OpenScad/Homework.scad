

$fn=60;
difference(){
    minkowski () {
        translate([-7,-7,0]) cube([14,14,5]);
        sphere(r=4);
    }
    translate([-15,-15,5]) cube([30,30,5]);
    translate([-15,-15,-5]) cube([30,30,5]);
    translate([0, 0, 1]) cylinder(h=5,r=8);
    translate([0, 0, 4]) cylinder(h=5,r=9);
    translate([0,7,2.5]) rotate([-90, 0, 0]) cylinder(h=4,r=1);
    
    translate([8, 8, -1]) cylinder(h=7,r=1);
    translate([-8, 8, -1]) cylinder(h=7,r=1);
    translate([8, -8, -1]) cylinder(h=7,r=1);
    translate([-8, -8, -1]) cylinder(h=7,r=1);
};
