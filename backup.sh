#!/bin/bash

function getArchiveName {
    dateString="$(date +"%Y-%m-%d")"
    fileString="Backup-${dateString}"

    count=1
    while [ -f "${fileString}.zip" ] ; do
        fileString="Backup-${dateString}_${count}"
        count=$(($count + 1))
    done

    echo "${fileString}.zip"
}

if ! [ -d "$1" ] ; then
    echo "Directory does not exist"!
    exit
fi

echo "Generating Filename..."

fileName="$(getArchiveName)"
echo "Filename: ${fileName}"

zip -9 -r "${fileName}" "$1"

echo "Done!"